import 'dotenv-flow/config';
import { createConnection, getConnection } from 'typeorm';
import { albumFixtures, artistFixtures, songFixtures } from '../test/setUp';

async function doFixture() {

    await createConnection({
        type: 'mysql',
        url: process.env.DATABASE_URL,
        synchronize: true,
        entities: ['src/entity/*.ts']
    });
    await getConnection().synchronize(true);
    

    await albumFixtures()
    await artistFixtures()
    await songFixtures()

    await getConnection().close();
}

doFixture();

