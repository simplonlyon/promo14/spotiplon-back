import { Column, Entity, JoinTable, ManyToMany, PrimaryGeneratedColumn } from "typeorm";
import { Artist } from "./Artist";
import { Song } from "./Song";


@Entity()
export class Album {

    @PrimaryGeneratedColumn()
    id:number;
    @Column()
    title:string;
    @Column()
    image:string;
    @Column({type:'date'})
    released:Date;
    
    @ManyToMany(()=> Song, song => song.albums, {onDelete: 'CASCADE'})
    @JoinTable()
    songs:Song[];
    
    @ManyToMany(() => Artist, artist => artist.albums, {onDelete: 'CASCADE'})
    artists:Artist[]
    
}