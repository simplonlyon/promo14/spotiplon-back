import { Column, Entity, ManyToMany, PrimaryGeneratedColumn } from "typeorm";
import { Album } from "./Album";
import { Artist } from "./Artist";


@Entity()
export class Song {

    @PrimaryGeneratedColumn()
    id:number;
    @Column()
    title:string;
    @Column()
    file:string;
    @Column({type:'date'})
    released:Date;
    
    @ManyToMany(() => Artist, artist => artist.songs, {onDelete: 'CASCADE'})
    artists:Artist[];
    @ManyToMany(() => Album, album => album.songs, {onDelete: 'CASCADE'})
    albums:Album[];
    

    
}