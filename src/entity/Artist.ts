import { Column, Entity, JoinTable, ManyToMany, PrimaryGeneratedColumn } from "typeorm";
import { Album } from "./Album";
import { Song } from "./Song";


@Entity()
export class Artist {

    @PrimaryGeneratedColumn()
    id:number;
    @Column()
    name:string;
    @Column()
    image:string;

    @ManyToMany(() => Album, album => album.artists, {onDelete: 'CASCADE'})
    @JoinTable()
    albums:Album[];

    @ManyToMany(() => Song, song => song.artists, {onDelete: 'CASCADE'})
    @JoinTable()
    songs:Song[];
    
    
    
}