import express from 'express';
import cors from 'cors';
import { albumController } from './controller/album-controller';
import morgan from 'morgan';

export const server = express();

server.use(morgan('dev'))
server.use(express.json())
server.use(cors());
server.use(express.static('public'));

server.use('/api/album', albumController);

// server.get('/fixtures', async (req, res) => {
//     await getConnection().synchronize(true);

//     await albumFixtures()
//     await artistFixtures()
//     await songFixtures()
//     res.end();
// })