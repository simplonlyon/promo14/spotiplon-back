import multer from 'multer';
import path from 'path';
import { randomUUID } from 'crypto';
import sharp from 'sharp';


const storage = multer.diskStorage({
    async destination(req,file,cb) {
        const uploadFolder =  __dirname+'/../public/uploads';
        cb(null,uploadFolder);
    },
    filename(req,file,cb) {
        cb(null, randomUUID()+path.extname(file.originalname))
        
    }
});

export const uploader = multer({
    storage,
    limits: {
        fileSize: 5000000
    },
    fileFilter(req, file, cb) {
        if(file.mimetype !== 'image/jpeg' && file.mimetype !== 'image/png') {
            cb(new Error('Invalid file type. Accepted files are : jpg, jpeg, png'))
        }
    }
});


export async function createThumbnail(file:Express.Multer.File, width=200, height=200) {
    const thumbnailFolder = __dirname+'/../public/uploads/thumbnails/';

    await sharp(file.path)
        .resize(width, height, {fit:'contain'})
        .toFile(thumbnailFolder+file.filename)
}