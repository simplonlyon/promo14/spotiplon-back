import { Router } from "express";
import { getRepository } from "typeorm";
import { Album } from "../entity/Album";
import { createThumbnail, uploader } from "../uploader";

export const albumController = Router();

albumController.get('/', async (req,res) => {

    try {
        
        const albums = await getRepository(Album).find({relations: ['artists']});

        res.json(albums);

    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
})




albumController.post('/', uploader.single('image'), async (req,res) => {
    try{
        let album = new Album();
        //Rajouter de la validation
        Object.assign(album, req.body);

        await createThumbnail(req.file);
        
        album.image = req.file.filename;

        await getRepository(Album).save(album);
        
        res.status(201).json(album);
    } catch(error) {
        console.log(error);
        res.status(500).json(error);
    }

});


albumController.get('/:id', async (req, res) => {
    try{
        const album = await getRepository(Album).findOne(req.params.id, {relations: ['artists', 'songs']});
        if(!album) {
            res.status(404).end();
            return;
        }
        res.json(album);
    } catch(error) {
        console.log(error);
        res.status(500).json(error);
    }
});

albumController.patch('/:id', async (req, res) => {
    try{
        const repo =  getRepository(Album);
        const album = await repo.findOne(req.params.id);
        if(!album) {
            res.status(404).end();
            return;
        } 
        Object.assign(album, req.body);
        repo.save(album);

        res.json(album);
    } catch(error) {
        console.log(error);
        res.status(500).json(error);
    }
})


albumController.delete('/:id', async (req, res) => {
    try{
        const repo =  getRepository(Album);
        const result = await repo.delete(req.params.id);
        if(result.affected !== 1) {
            res.status(404).end();
            return;
        } 
        

        res.status(204).end();
    } catch(error) {
        console.log(error);
        res.status(500).json(error);
    }
})
