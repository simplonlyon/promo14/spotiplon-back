import { createConnection, getManager, getConnection, DeepPartial } from "typeorm";
import { Album } from "../src/entity/Album";
import faker from 'faker';
import { Artist } from "../src/entity/Artist";
import { Song } from "../src/entity/Song";

/**
 * Petite fonction qui met en place une base de données de test en mémoire avec
 * sqlite (et plus spécifiquement la library better-sqlite3).
 * Cette db sera recréée avant chaque test et supprimer après chaque test, il faut donc
 * faire également en sorte d'y mettre des données de test si c'est nécessaire (les fonctions
 * de fixtures servent à ça)
 */
export function setUpTestDatabase() {

    beforeEach(async () => {
        await createConnection({
            type: 'better-sqlite3',
            database: ':memory:',
            synchronize: true,
            entities: ['src/entity/*.ts'],
            dropSchema: true
        });

        await albumFixtures()
        await artistFixtures()
        await songFixtures()
        
    });

    afterEach(async () => {
        await getConnection().close()
    })
}
/**
 * Fonction qui fait persister des album pour les tests. Il faudra la lancer
 * avant chaque test et en créer d'autres pour les autres entités à tester.
 */
export async function albumFixtures() {
    const albums:DeepPartial<Album>[] = [];
    for (let index = 0; index < 10; index++) {
        albums.push({
            title: faker.name.title(),
            image: faker.image.nature(),
            released: faker.date.past()
        })
        
    }
    await getManager().save(Album, albums);
}

export async function artistFixtures() {
    const artists:DeepPartial<Artist>[] = [];
    for (let index = 1; index <= 10; index++) {
        artists.push({
            image: faker.image.avatar(),
            name: faker.name.firstName(),
            albums: [
                {id: index}
            ]
        })
        
    }
    await getManager().save(Artist, artists);
}

export async function songFixtures() {
    const songs:DeepPartial<Song>[] = [];
    for (let index = 1; index <= 10; index++) {
        for(let x = 1; x <= 10; x++ ){
            songs.push({
                file: '/fakefile.mp3',
                title: faker.random.words(2),
                released: faker.date.past(),
                artists: [
                    {id: index}
                ],
                albums: [
                    {id: index}
                ]
            })
        }
        
    }
    await getManager().save(Song, songs);
}
