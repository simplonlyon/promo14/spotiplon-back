import { setUpTestDatabase } from "./setUp";
import request from 'supertest';
import { server } from "../src/server";
import { getManager } from "typeorm";
import { Album } from "../src/entity/Album";
import { readFileSync } from "fs";


describe('Album controller', () => {

    setUpTestDatabase();

    it('should return the albums on get', async () => {
        const response = await request(server)
        .get('/api/album')
        .expect(200);

        expect(response.body).toContainEqual({
            id: expect.any(Number),
            title: expect.any(String),
            released: expect.any(String),
            image: expect.any(String),
            artists: expect.anything()
        });
    })

    //Upload ne marche pas
    // it('should add a new album on post', async () => {
    //     const file = readFileSync(__dirname+'/image.png');
    //     expect(file).toBeDefined();

    //     const response = await request(server)
    //     .post('/api/album')
    //     .field('Content-Type', 'multipart/form-data')
    //     .field('title', 'Test Title')
    //     .field('released', '2021-08-20')
    //     .attach('image', file, 'image.png')
    //     .expect(201);

    //     expect(response.body).toEqual({
    //         id: expect.any(Number),
    //         title: expect.any(String),
    //         released: expect.any(String),
    //         image: expect.any(String),
            
    //     });
    //     //Compter qu'on a bien une album de plus en bdd après le post
    //     //pas obligé, mais pourquoi pas
    //     expect(await getManager().count(Album)).toBe(11);
    // })

    it('should return a album on get with id', async () => {
        const response = await request(server)
        .get('/api/album/1')
        .expect(200);

        expect(response.body).toEqual({
            id: expect.any(Number),
            title: expect.any(String),
            released: expect.any(String),
            image: expect.any(String),
            artists: expect.anything(),
            songs: expect.anything()
        });
    });

    it('should return a 404 on unexisting album on GET/PATCH/DELETE', async () => {
        await request(server)
        .get('/api/album/100')
        .expect(404);
        await request(server)
        .patch('/api/album/100')
        .expect(404);
        await request(server)
        .delete('/api/album/100')
        .expect(404);

    });

    it('should remove a album on delete with id', async () => {
        await request(server)
        .delete('/api/album/1')
        .expect(204);

        expect(await getManager().count(Album)).toBe(9);

    })

    it('should modify a album on patch', async () => {
        const response = await request(server)
        .patch('/api/album/1')
        .send({
            title: 'fromtest'
        })
        .expect(200);

        expect(response.body).toEqual({
            id: 1,
            title: 'fromtest',
            released: expect.any(String),
            image: expect.any(String)
        });
    })
})
